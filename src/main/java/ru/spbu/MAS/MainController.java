package ru.spbu.MAS;

import jade.core.Profile;
import jade.core.ProfileImpl;
import jade.core.Runtime;
import jade.wrapper.AgentController;
import jade.wrapper.ContainerController;

class MainController {

    private static final String[] agents_nicknames = {"agent0", "agent1", "agent2", "agent3", "agent4"};

    void initAgents() {
        Runtime rt = Runtime.instance();

        Profile p = new ProfileImpl();
        p.setParameter(Profile.MAIN_HOST, "localhost");
        p.setParameter(Profile.MAIN_PORT, "10098");
        p.setParameter(Profile.GUI, "true");
        ContainerController agentController = rt.createMainContainer(p);

        try {
            for (String name : agents_nicknames) {
                AgentController agent = agentController.createNewAgent(name, FindAverageAgent.class.getCanonicalName(), null);
                agent.start();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}