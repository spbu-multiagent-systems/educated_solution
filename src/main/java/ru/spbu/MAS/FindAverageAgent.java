package ru.spbu.MAS;
import jade.core.Agent;
import java.util.HashMap;

public class FindAverageAgent extends Agent {
    public String agent_name;
    public HashMap<String, Double> agentsLinkProbabilities;
    public Double value;

    private static final double[][] B_av = {
            {0, 1, 0, 0, 0, 0},
            {1, 0, 0.5, 1, 0, 0},
            {0, 0.5, 0, 1, 0, 0},
            {0, 1, 1, 0, 1, 0},
            {0, 0, 0, 1, 0, 1},
            {0, 0, 0, 0, 1, 0}
    };

    private static double getAgentValueByName(String agentName) {
        switch (agentName) {
            case "agent0":
                return 8;
            case "agent1":
                return 22;
            case "agent2":
                return 15;
            case "agent3":
                return 5;
            case "agent4":
                return 0;
            case "agent5":
                return 10;
            default:
                throw new RuntimeException("No agent with this name");
        }
    }

    String getAgentNameForId(int id) {
        return "agent" + id;
    }

    int getIdForAgentName(String agentName) {
        return Character.getNumericValue(agentName.charAt(agentName.length() - 1));
    }

    double getConnectionProbability(int forAgent, int toAgent) {
        return B_av[forAgent][toAgent];
    }

    @Override
    protected void setup() {
        this.agent_name = getAID().getLocalName();
        this.value = getAgentValueByName(this.agent_name);
        this.agentsLinkProbabilities = new HashMap<>();

        int id = getIdForAgentName(agent_name);
        for (int connectedId = 0; connectedId < B_av[id].length; connectedId++) {
            if (B_av[id][connectedId] != 0) {
                this.agentsLinkProbabilities.put(getAgentNameForId(connectedId), getConnectionProbability(id, connectedId));
            }
        }

        addBehaviour(new FindAverageBehaviour(this, Constants.tickLenMs));
        Logger.log("Agent <" + agent_name + "> initialized", true);
    }
}
