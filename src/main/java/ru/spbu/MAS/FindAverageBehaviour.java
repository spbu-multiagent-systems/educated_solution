package ru.spbu.MAS;

import jade.core.AID;
import jade.core.behaviours.TickerBehaviour;
import jade.lang.acl.ACLMessage;

import java.util.HashMap;

public class FindAverageBehaviour extends TickerBehaviour {
    private final FindAverageAgent agent;

    FindAverageBehaviour(FindAverageAgent agent, long period) {
        super(agent, period);
        this.setFixedPeriod(true);
        this.agent = agent;
    }

    @Override
    protected void onTick() {
        Logger.log("Agent " + this.agent.getLocalName() + ": TICK = " + getTickCount(), false);

        ACLMessage msg = new ACLMessage(ACLMessage.INFORM);

        for (HashMap.Entry<String, Double> entry : this.agent.agentsLinkProbabilities.entrySet()) {
            String agent = entry.getKey();
            Double linkProbability = entry.getValue();
            if (linkProbability > Math.random()) {
                AID destination = new AID(agent, AID.ISLOCALNAME);
                msg.addReceiver(destination);
            }
        }

        try { // value + noise
            msg.setContentObject(this.agent.value + 2 * (Math.random() - 0.5));
        } catch (Exception e) {
            e.printStackTrace();
        }

        Runnable sendMsg = () -> this.agent.send(msg);
        int delay = Math.random() < Constants.delayProbability ? (int) (Constants.maxDelayMs * Math.random()) : 0;

        new java.util.Timer().schedule(
                new java.util.TimerTask() {
                    @Override
                    public void run() {
                        sendMsg.run();
                    }
                },
                delay
        );

        long endTime = System.currentTimeMillis() + Constants.tickLenMs;
        while (System.currentTimeMillis() < endTime) {
            ACLMessage receivedMsg = this.agent.receive();
            if (receivedMsg == null) {
                continue;
            }
            try {
                Double neighborValue = (Double) receivedMsg.getContentObject();
                this.agent.value += Constants.alpha * (neighborValue - this.agent.value);
                Logger.log("Content " + this.agent.value, false);
            } catch (Exception e) { /* if destination agent is stopped */ }
        }

        if (getTickCount() == Constants.totalTicks) {
            Logger.log(this.agent.getLocalName() + ":\n" + "TICK: " + getTickCount() + "\n" + "Value: " + this.agent.value + "\n", true);

            this.agent.doDelete();
            this.stop();
        }
    }
}