package ru.spbu.MAS;

public class Constants {
    public static int tickLenMs = 100;
    public static int totalTicks = 200;
    public static double alpha = 0.05;
    public static int maxDelayMs = (int) (tickLenMs * 1.1);
    public static double delayProbability = 0.3;
}
